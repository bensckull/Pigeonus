Pour lancer la résolution sur un tableau a 2 dimension : 

test -m 1 -n 4 -q 4

    -n nombre de pigeons
    -q nombre de pigeniers 
    
Pour lanceer la résolution sur n dimmensions (non effectif actuellemet)

test -m 2 -c 4 0 1 0 4 -c 4 1 1 4 4

    -c pour definir les contraites pour une dimmetion 
    explicati0n en prenant pour exemple la ligne de commande
    celle-ci représente la résolution pigeon pigeoniers
    
    4 taille de la dimension ( nb de pigeoniers)
    0 nombre mini d'affectation par ligne (au miniumum 0 pigeon par pigeonier)
    1 nombre maxi d'affectation par ligne (au maximum 1 pigeon par pigeonier)
    0 nombre mini d'affectation totale (au minimum aucun pigeonier ne contient de pigeons)
    4 nombre maxi d'affectation totale (au maximum 4 pigeon affectés pour les pigeniers)
    
    4 taille de la dimension ( nb de pigeons)
    1 nombre mini d'affectation par ligne (au miniumum 1 pigeon par pigeonier)
    1 nombre maxi d'affectation par ligne (au maximum 1 pigeon par pigeonier)
    4 nombre mini d'affectation totale (au minimum 4 pigeon sont affectés)
    4 nombre maxi d'affectation totale (au maximum 4 pigeon sont affectés)

utiliser l'argument -t pour tester la traductiond'indices  de la matrice a une 
dim vers une matrice à n dim

exemple : test -m 2 -c 3 ... -c 3 ... -c 3 ... -t 26

            [26] => [2][2][2]

fonctionnement de la traduction d'indice de matrice : 

    [[[1,2,3],[4,5,6],[7,8,9]],[[10,11,12],[13,14,15],[16,17,18]],[[19,20,21],[22,23,24],[25,26,27]]]

    [
        0[     0  1  2
            0[ 1, 2, 3],
            1[ 4, 5, 6],
            2[ 7, 8, 9]
        ],
        1[    0   1  2
            0[10,11,12],
            1[13,14,15],
            2[16,17,18]
        ],
        2[     0  1  2
            0[19,20,21],
            1[22,23,24],
            2[25,26,27]
        ]
    ]


    [0][0][0] => [0(3*3)+0(3)+0] => [0] =>  1

    [1][1][1] => [1(3*3)+1(3)+1] => [13] => 14

    [2][2][2] => [2(3*3)+2(3)+2] => [26] => 27

    26 => 26/(3*3)     = 2,...
          26 mod (3*3) = 8
      
          8/(3)        = 2,...
          8 mod 3      = 2
      
         [2][2][2]
