

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <iterator>
#include <algorithm>
#include <omp.h>
#include "ressolv.hpp"
#include "constraints.hpp"
#include "ressolvND.hpp"
#include "ressolvNDOPT.hpp"

using namespace std;

int pg_num = 0;
int pgnier_num = 0;
int ** r;
int * rND;
int indTest = -1;

/*----------------------------------------
affichage de la matrice

    t matrice à afficher
    x nombre de lignes de la matrice
    y nombre de colonnes de la matrice
----------------------------------------*/
void print_table(int ** t, int x, int y){

    for (int i = 0; i < x; ++i)
    {
        for (int j = 0; j < y; ++j)
        {
            std::cout << t[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout <<"-------------------"<< std::endl;

}

void printNDmatrix(int * t, Constraints c){
//    int nbd = c.getNbDim();
    int nbv = c.getNbVar();

    for (int i = 0; i < nbv; ++i)
    {
        std::vector<int> v = c.convertToN(i);

        for(int ind : v){
            cout << "[" << ind << "]";
        }

        cout << ": " << t[i] << endl;

    }

    cout << endl ;
}

/*-------------------------------------
fonction de copie d'une matrice
    t matrice à copier

    return : A copie de la matrice
-------------------------------------*/
int ** copyMatrix(int ** t){

    int **A = new int*[pgnier_num];

    for (int i = 0; i < pgnier_num; i++){
        A[i] = new int[pg_num];
    }

    for (int i = 0; i < pgnier_num; ++i)
    {
        for (int j = 0; j < pg_num; ++j)
        {
            A[i][j] = t[i][j];

        }
    }
    return A;
}

/*-------------------------------------
fonction de copie d'une matrice
    t matrice à copier

    return : A copie de la matrice
-------------------------------------*/
int * copyNDMatrix(int * t, Constraints c){
    int max = c.getNbVar();

    int * A = new int[max];

    for (int i = 0; i < max; ++i)
    {
        A[i] = t[i];
    }

    return A;
}

/*-------------------------------------
fonction optimisé de copie d'une matrice
    t matrice à copier

    return : A copie de la matrice
-------------------------------------*/
int * copyNDOPTMatrix(int * t, Constraints c){
    int max = c.getNbVar();

    int * A = new int[max];

    for (int i = 0; i < max; ++i)
    {
        A[i] = t[i];
    }

    return A;
}

/*--------------------------------------------------
  Creation matrice de resolution et initialisation
  d'une solution
--------------------------------------------------*/
int** createTestMatrix(){
    int **A = new int*[pgnier_num];

    for (int i = 0; i < pgnier_num; i++){
        A[i] = new int[pg_num];
    }

    for (int i = 0; i < pgnier_num; ++i)
    {
        for (int j = 0; j < pg_num; ++j)
        {
            if(i == j){
                A[i][j] = 1;
            }else{
                A[i][j] = 0;
            }
        }
    }

    return A;
}


/*---------------------------------------
Creation d'une matrice initialisé à 0

    return : A la matrice initialisé
---------------------------------------*/

int ** createEmptyMatrix(){
    int **A = new int*[pgnier_num];

    for (int i = 0; i < pgnier_num; i++){
        A[i] = new int[pg_num];
    }

    for (int i = 0; i < pgnier_num; ++i)
    {
        for (int j = 0; j < pg_num; ++j)
        {
            A[i][j] = 0;
        }
    }

    return A;
}

/*--------------------------------------------------------------------------------
    Fonction de création d'une matrice à une dimension représentant
    une matrice à N Dimmensions

    c contraintes passés en paramètres à l'exécutions contenant le nombre
       de variables à crééer

    return : A la matrice généré
--------------------------------------------------------------------------------*/
int * createEmptyNDMatrix(Constraints c){
    int max = c.getNbVar();

    int * A = new int[max];

    for (int i = 0; i < max; ++i)
    {
        A[i] = 0;
    }

    return A;

}

/*--------------------------------------------------------------------------------
    Fonction optimisé de création d'une matrice à une dummesion représentant
    une matrice à N Dimmensions

    c contraintes passés en paramètres à l'exécutions contenant le nombre
       de variables à crééer

    return : A la matrice généré
--------------------------------------------------------------------------------*/
int * createEmptyNDOPTMatrix(Constraints c){
    int max = c.getNbVar();

    int * A = new int[max];

    for (int i = 0; i < max; ++i)
    {
        A[i] = 0;
    }

    return A;

}

/*------------------------------------------------------
 Fonction de vérification du résultat sur la matrice
    t matrice a verifier
    p pigeoniers
    n pigeons

    return : vrai si la matrice est uen solution, false sinon
------------------------------------------------------*/
bool solved(int ** t,  int p, int n){
    /*nombre total de pigeons affectés */
    int nb_pg_affect = 0;
    /*nombre de pigeon affecté pour le pigeeonier courrant*/
    int nbaffectbypgn;
    /*liste des pigeons affectés */
    std::vector<int> v;

    for (int i = 0; i < p; ++i)
    {
        nbaffectbypgn = 0;
        for (int j = 0; j < n; ++j)
        {
            /*si tout le matrice n'a pas été définie*/
            if(t[i][j] == -1){

                return false;

            }else if(t[i][j] == 1){

                /*verif si le pigeon courrant est déja affecté*/
                if(std::find(v.begin(), v.end(), j)==v.end()){
                    /*s'il ne l'est pas */
                    nb_pg_affect ++;
                    nbaffectbypgn ++;

                    v.push_back(j);

                }else{
                    /*s'il l'est*/

                    return false;
                }
            }
        }

        /*si le poigeonier a plus d'un pigeon affecté*/
        if(nbaffectbypgn > 1){

            return false;
        }
    }

    /*si le nombre de pigons affecté es différent du nombre total de pigeons*/
    if(nb_pg_affect != n){

        return false;
    }

    return true;
}

/*------------------------------------------------------
 Fonction de vérification du résultat sur la matrice
    t matrice a verifier
    p pigeoniers
    n pigeons

    return : résultat res de type Resolv contenant le résultat de la résolution
------------------------------------------------------*/

Ressolv solved2(int ** t,  int p, int n){
    /*nombre total de pigeons affectés */
    int nb_pg_affect = 0;

    /*Objet effectuant la vérification de la matrice et contenant le résultat*/
    Ressolv res;

    for (int i = 0; i < p; ++i)
    {
        for (int j = 0; j < n; ++j)
        {

            if(t[i][j] == 1){

                /*verif si le pigeon courrant est déja affecté*/
                if(res.addCol(j)){
                    /*s'il ne l'est pas */
                    nb_pg_affect ++;

                }else{
                    /*s'il l'est*/

                    return res;
                }

                /*verif si le pigeonier a plus d'un pigeon affecté */
                if(!res.addLine(i)){
                    return res;
                }

            }
        }
    }

    /*si le nombre de pigons affecté es différent du nombre total de pigeons*/
    if(nb_pg_affect != n){

        return res;
    }

    res.setResloved();
    return res;
}

/*--------------------------------------------------------------------------------
 Fonction de vérification du résultat sur la matrice sur N Dimmensions
    t matrice a verifier
    c contraintes de résolution du problème

    return : résultat res de type ResolvND contenant le résultat de la résolution
--------------------------------------------------------------------------------*/
RessolvND solvedND(int *t, Constraints c){

    /*nombre total de variables instenciés */
    int nnbv = c.getNbVar();

    /*Objet effectuant la vérification de la matrice et contenant le résultat*/
    RessolvND res;

    /*ajout des contraintes à res*/
    res.setConstr(c);

    for (int i = 0; i < nnbv; ++i)
    {

        if(t[i] == 1){
            /*verif si la variable conrrespond aux contraintes*/
            if(res.add(i)){

                /*si oui on continue*/
            }else{
                /*si elle ne correspond pas */

                return res;
            }
        }
    }

    /*calcul du resultat avec les dernières contraintes (les contraintes min)*/
    res.setResloved();
    return res;
}

/*--------------------------------------------------------------------------------
 Fonction optimisée de vérification du résultat sur la matrice sur N Dimmensions
    t matrice a verifier
    c contraintes de résolution du problème

    return : résultat res de type ResolvND contenant le résultat de la résolution
--------------------------------------------------------------------------------*/
RessolvNDOPT solvedNDOPT(int *t, Constraints c){

    /*nombre total de variables instenciés */
    int nnbv = c.getNbVar();

    /*Objet effectuant la vérification de la matrice et contenant le résultat*/
    RessolvNDOPT res;

    /*ajout des contraintes à res*/
    res.setConstr(c);

    #pragma omp parallel for shared(res)
    for (int i = 0; i < nnbv; ++i)
    {

        if(t[i] == 1){
            /*verif si la variable conrrespond aux contraintes*/
            if(res.add(i)){

                /*si oui on continue*/
            }else{
                /*si elle ne correspond pas */

                //return res;
                i = nnbv;
            }
        }
    }

    /*calcul du resultat avec les dernières contraintes (les contraintes min)*/
    res.setResloved();
    return res;
}


/*------------------------------------------------------
 Fonction de vérification du résultat sur la matrice
    t matrice pigeoniers * pigeons
    p pigeoniers
    n pigeons
    pi pigeonier courrant
    ni pigeon courrant

    r matrice resultat definie en global

    return : true si la matrice représente une solution , false sinon
------------------------------------------------------*/
bool solve(int **t, int pi, int ni, int p , int n){
    /*-------------------------------------------------
        declaration et initialisation des vzriables
    -------------------------------------------------*/

    int ** k = copyMatrix(t);
    bool bsolved = false;
    int pisuiv, nisuiv;

    /*-----------------------------------------------
        gestion des déplacements des la matrice
    -----------------------------------------------*/
    if(ni+1 < n){
        nisuiv = ni+1;
        pisuiv = pi;
    }else{
        nisuiv = 0;
        pisuiv = pi+1;
    }

    /*si position de la prochiane variable à traiter en dehors de la matrice
    la matrice précédente n'est pas une solution
    supression de la matrice en mémoire  return false*/
    if(pi >= p){
        for (int i = 0; i < p; ++i)
        {
            delete[] k[i];
        }

        delete[] k;

        return false;
    }


    /*-------------------------------------------
        définition de la variable courrante à 0
    --------------------------------------------*/
    k[pi][ni] = 0;

    if(solved(k, p, n)){
        /*si probleme resolu, copie de la matrice dans r */

        r = copyMatrix(k);
        return true;
    }else{
        /*autrement, modification de la variable suivante
        et execution de la fonction sur la nouvelle matrice*/

        bsolved = solve(k, pisuiv, nisuiv, p, n);
        if(bsolved){
            return true;
        }
    }

    /*-------------------------------------------
        définition de la variable courrante à 1
    --------------------------------------------*/
    k[pi][ni] = 1;


    if(solved(k, p, n)){
        /*si probleme resolu, copie de la matrice dans r */

        r = copyMatrix(k);
        return true;

    }else{
        /*autrement, modification de la variable suivante
        et execution de la fonction sur la nouvelle matrice*/

        bsolved = solve(k, pisuiv, nisuiv, p, n);
        if(bsolved){

            return true;
        }
    }


    /*la ma trice n'est pas une solution, suppression de son allocation mémoire*/
    for (int i = 0; i < p; ++i)
        {
            delete[] k[i];
        }

        delete[] k;
    return false;
}

/*------------------------------------------------------
 Fonction de vérification du résultat sur la matrice
    t matrice pigeoniers * pigeons
    p pigeoniers
    n pigeons
    pi pigeonier courrant
    ni pigeon courrant

    r matrice resultat definie en global

    return : résultat res de type ResolvND contenant le résultat de la résolution
------------------------------------------------------*/
Ressolv solve2(int **t, int pi, int ni, int p , int n){
    /*-------------------------------------------------
        declaration et initialisation des vzriables
    -------------------------------------------------*/

    int ** k = copyMatrix(t);
    Ressolv bsolved;
    int pisuiv, nisuiv;

    /*-----------------------------------------------
        gestion des déplacements des la matrice
    -----------------------------------------------*/
    if(ni+1 < n){
        nisuiv = ni+1;
        pisuiv = pi;
    }else{
        nisuiv = 0;
        pisuiv = pi+1;
    }

    /*si position de la prochiane variable à traiter en dehors de la matrice
    la matrice précédente n'est pas une solution
    supression de la matrice en mémoire  return false*/
    if(pi >= p){
        for (int i = 0; i < p; ++i)
        {
            delete[] k[i];
        }

        delete[] k;

        return bsolved;
    }


    /*-------------------------------------------
        définition de la variable courrante à 0
    --------------------------------------------*/
    k[pi][ni] = 0;

    bsolved = solved2(k, p, n);
    if(bsolved.isResolved()){
        /*si probleme resolu, copie de la matrice dans r */

        r = copyMatrix(k);
        return bsolved;
    }else{

        /*autrement, modification de la variable suivante
        et execution de la fonction sur la nouvelle matrice*/

        bsolved = solve2(k, pisuiv, nisuiv, p, n);
        if(bsolved.isResolved()){
            return bsolved;
        }
    }

    /*-------------------------------------------
        définition de la variable courrante à 1
    --------------------------------------------*/
    k[pi][ni] = 1;

    bsolved = solved2(k, p, n);
    if(bsolved.isResolved()){
        /*si probleme resolu, copie de la matrice dans r */

        r = copyMatrix(k);
        return bsolved;

    }else{
        /*autrement, modification de la variable suivante
        et execution de la fonction sur la nouvelle matrice*/

        bsolved = solve2(k, pisuiv, nisuiv, p, n);
        if(bsolved.isResolved()){

            return bsolved;
        }
    }


    /*la matrice n'est pas une solution, suppression de son allocation mémoire*/
    for (int i = 0; i < p; ++i)
        {
            delete[] k[i];
        }

        delete[] k;
    return bsolved;
}

/*------------------------------------------------------
 Fonction de vérification du résultat sur la matrice
    t matrice a une dimmensions représnetent un ma trise a n dimmensions
    c contraintes
    pos position courrante dans la matrice

    rND matrice resultat definie en global

    return : résultat res de type ResolvND contenant le résultat de la résolution
------------------------------------------------------*/
RessolvND solveND(int *t, int pos, Constraints c){
    /*-------------------------------------------------
        declaration et initialisation des variables
    -------------------------------------------------*/

    int * k = copyNDMatrix(t,c);
    RessolvND bsolved;
    int posSuiv = pos+1;

    /*-----------------------------------------------
        gestion des déplacements des la matrice
    -----------------------------------------------*/

    /*si position de la prochiane variable à traiter en dehors de la matrice
    la matrice précédente n'est pas une solution
    supression de la matrice en mémoire  return false*/
    if(pos >= c.getNbVar()){

        delete[] k;

        return bsolved;
    }



    /*-------------------------------------------
        définition de la variable courrante à 0
    --------------------------------------------*/

    k[pos] = 0;


    bsolved = solvedND(k,c);

    if(bsolved.isResolved()){
        /*si probleme resolu, copie de la matrice dans r */

        rND = copyNDMatrix(k,c);
        return bsolved;
    }else{
        /*autrement, modification de la variable suivante
        et execution de la fonction sur la nouvelle matrice*/

        bsolved = solveND(k, posSuiv ,c);
        if(bsolved.isResolved()){
            return bsolved;
        }
    }

    /*-------------------------------------------
        définition de la variable courrante à 1
    --------------------------------------------*/

    k[pos] = 1;

    bsolved = solvedND(k,c);
    if(bsolved.isResolved()){
        /*si probleme resolu, copie de la matrice dans r */

        rND = copyNDMatrix(k,c);
        return bsolved;

    }else{
        /*autrement, modification de la variable suivante
        et execution de la fonction sur la nouvelle matrice*/

        bsolved = solveND(k, posSuiv ,c);
        if(bsolved.isResolved()){

            return bsolved;
        }
    }


    /*la matrice n'est pas une solution, suppression de son allocation mémoire*/
    delete[] k;

    return bsolved;
}

/*------------------------------------------------------
 Fonction optimisé de vérification du résultat sur la matrice
    t matrice a une dimmensions représnetent un ma trise a n dimmensions
    c contraintes
    pos position courrante dans la matrice

    rND matrice resultat definie en global

    return : résultat res de type ResolvND contenant le résultat de la résolution
------------------------------------------------------*/
RessolvNDOPT solveNDOPT(int *t, int pos, Constraints c){
    /*-------------------------------------------------
        declaration et initialisation des variables
    -------------------------------------------------*/

    int * k = copyNDOPTMatrix(t,c);
    RessolvNDOPT bsolved;
    int posSuiv = pos+1;

    /*-----------------------------------------------
        gestion des déplacements des la matrice
    -----------------------------------------------*/

    /*si position de la prochiane variable à traiter en dehors de la matrice
    la matrice précédente n'est pas une solution
    supression de la matrice en mémoire  return false*/
    if(pos >= c.getNbVar()){

        delete[] k;

        return bsolved;
    }



    /*-------------------------------------------
        définition de la variable courrante à 0
    --------------------------------------------*/

    k[pos] = 0;


    bsolved = solvedNDOPT(k,c);

    if(bsolved.isResolved()){
        /*si probleme resolu, copie de la matrice dans r */

        rND = copyNDOPTMatrix(k,c);
        return bsolved;
    }else{
        /*autrement, modification de la variable suivante
        et execution de la fonction sur la nouvelle matrice*/

        bsolved = solveNDOPT(k, posSuiv ,c);
        if(bsolved.isResolved()){
            return bsolved;
        }
    }

    /*-------------------------------------------
        définition de la variable courrante à 1
    --------------------------------------------*/

    k[pos] = 1;

    bsolved = solvedNDOPT(k,c);
    if(bsolved.isResolved()){
        /*si probleme resolu, copie de la matrice dans r */

        rND = copyNDOPTMatrix(k,c);
        return bsolved;

    }else{
        /*autrement, modification de la variable suivante
        et execution de la fonction sur la nouvelle matrice*/

        bsolved = solveNDOPT(k, posSuiv ,c);
        if(bsolved.isResolved()){

            return bsolved;
        }
    }


    /*la matrice n'est pas une solution, suppression de son allocation mémoire*/
    delete[] k;

    return bsolved;
}

int * intToBinArrayOPT(int num, Constraints c){
    int max = c.getNbVar();

    int * A = new int[max];

    int r,i = 0;
    while(i<max){
        r = num%2;
        A[i] = r;
        num /= 2;

        i++;
    }

    return A;

}

int * intToBinArray(unsigned long num, Constraints c){
    int max = c.getNbVar();

    int * A = new int[max];

    unsigned long r,i = 0;

    #pragma omp parallel
    while(i<(unsigned long)max){
        r = num%2;
        A[i] = r;
        num /= 2;

        i++;
    }

    return A;

}

RessolvND solveNDnoRecurs(Constraints c){


    RessolvND bsolved;
    int loopsize = pow(2,c.getNbVar());;


    for (int i = 0; i < loopsize; ++i)
    {
        int * A = intToBinArray(i,c);

        RessolvND bs = solvedND(A,c);


        if(bs.isResolved()){

            rND = A;
            bsolved = bs;
            i = loopsize;
        }else{
            delete [] A;
        }

    }

    return bsolved;
}

RessolvNDOPT solveNDOPTnoRecurs(Constraints c){


    RessolvNDOPT bsolved;
    //unsigned int loopsize = pow(2,c.getNbVar());
    unsigned long loopsize = (unsigned long)pow(2,c.getNbVar());

    //cout << "loopsize "  << loopsize << endl;
    #pragma omp parallel for
    for (unsigned long i = 0; (unsigned long)i < loopsize; ++i)
    {
        //cout << "i:" << i << " ";
        int * A = intToBinArrayOPT(i,c);

        //#pragma omp critical
        RessolvNDOPT bs = solvedNDOPT(A,c);

        #pragma omp critical
        if(bs.isResolved()){

            rND = A;
            bsolved = bs;
            i = loopsize;
        }else{
            delete [] A;
        }

    }
    //cout << "sortie"<< endl;
    return bsolved;
}


int main(int argc, char ** argv) {

    /*----------------------
     vars init
    ----------------------*/

    //version 1
    int pg_num = 0;
    int pgnier_num = 0;

    int mode = 0;
    int conv = 0;

    //version 2 ND
    Constraints constr;
    int nbdim = 0;

    /*-----------------------/
      Arguments
    -----------------------*/

    for ( int i = 1; i < argc; ++i ) {
        if(!strcmp(argv[i],"-n")){
            /*
            Nombre de pigeons
            */
            conv = atoi(argv[++i]);
            pg_num = conv;

        }else if(!strcmp(argv[i],"-q")){
            /*
            Nombre de pigeonier
            */
            conv = atoi(argv[++i]);
            pgnier_num = conv;

        }else if(!strcmp(argv[i],"-m")){

            conv = atoi(argv[++i]);
            mode = conv;

        }else if(!strcmp(argv[i],"-nb")){

            conv = atoi(argv[++i]);
            constr.setNbDim(conv);

        }else if(!strcmp(argv[i],"-c")){

                conv = atoi(argv[++i]);
                constr.setMaxDim(conv);

                conv = atoi(argv[++i]);
                constr.setMinOccur(conv);

                conv = atoi(argv[++i]);
                constr.setMaxOccur(conv);

                conv = atoi(argv[++i]);

                constr.setMinTrue(conv);
                conv = atoi(argv[++i]);

                constr.setMaxTrue(conv);

                nbdim++;

        }else if(!strcmp(argv[i],"-t")){

            conv = atoi(argv[++i]);
            indTest = conv;

        }
    }

    constr.setNbDim(nbdim);
    constr.initSizeDim();

    /*---------------------------
        programme principal
    ---------------------------*/

    int ** A = createEmptyMatrix();
    if(mode == 0){
        bool res = solve(A, 0, 0, pgnier_num, pg_num);

        cout << "resutat: " << res << endl;

        if(res){
            print_table(r, pgnier_num, pg_num);
        }

    }else if(mode ==1){
        Ressolv res = solve2(A, 0, 0, pgnier_num, pg_num);

        cout << "resutat: " << res.isResolved() << endl;

        if(res.isResolved()){
            print_table(r, pgnier_num, pg_num);
        }

        res.printVect();



    }else if (mode == 2){

        int ntest = constr.getNbDim();
        cout << "nb dim : \t\t"<< ntest << endl;
        cout << "nb var : \t\t"<< constr.getNbVar() << endl << endl;

        for(int i=0; i<ntest; i++){
            cout << "indic vector : \t\t"<< i << endl;
            cout << "max dim : \t\t" << constr.getMaxDim(i) << endl;
            cout << "min occ : \t\t" << constr.getMinOccur(i) << endl;
            cout << "max occ : \t\t" << constr.getMaxOccur(i) << endl;
            cout << "Min in dim : \t\t" << constr.getMinTrue(i) << endl;
            cout << "Max in dim : \t\t" << constr.getMaxTrue(i) << endl;
            cout << "Size Dim : \t\t"<< constr.getSizeDim(i) << endl;
            cout << "tot Max in dim : \t" << constr.getMaxTotDim(i) << endl << endl;
        }

        if(indTest >= 0){
            cout << "test convert vector indice : " << endl;
            cout <<"["<< indTest << "] => ";

            std::vector<int> v = constr.convertToN(indTest);

            for(int ind : v){
                cout << "[" << ind << "]";
            }
            cout << endl;
        }

        int * ND = createEmptyNDMatrix(constr);

        RessolvND res = solveND(ND, 0, constr);


        if(res.isResolved()){
            cout << "resolu !!!!!!" << endl << endl;
            printNDmatrix(rND, constr);

        }else{
            cout << "fail" << endl;
        }

    }else{

        int ntest = constr.getNbDim();
        cout << "nb dim : \t\t"<< ntest << endl;
        cout << "nb var : \t\t"<< constr.getNbVar() << endl << endl;

        for(int i=0; i<ntest; i++){
            cout << "indic vector : \t\t"<< i << endl;
            cout << "max dim : \t\t" << constr.getMaxDim(i) << endl;
            cout << "min occ : \t\t" << constr.getMinOccur(i) << endl;
            cout << "max occ : \t\t" << constr.getMaxOccur(i) << endl;
            cout << "Min in dim : \t\t" << constr.getMinTrue(i) << endl;
            cout << "Max in dim : \t\t" << constr.getMaxTrue(i) << endl;
            cout << "Size Dim : \t\t"<< constr.getSizeDim(i) << endl;
            cout << "tot Max in dim : \t" << constr.getMaxTotDim(i) << endl << endl;
        }

        if(indTest >= 0){
            cout << "test convert vector indice : " << endl;
            cout <<"["<< indTest << "] => ";

            std::vector<int> v = constr.convertToN(indTest);

            for(int ind : v){
                cout << "[" << ind << "]";
            }
            cout << endl;
        }

        //int * ND = createEmptyNDOPTMatrix(constr);

        //RessolvNDOPT res = solveNDOPT(ND, 0, constr);
        RessolvNDOPT res = solveNDOPTnoRecurs(constr);


        if(res.isResolved()){
            cout << "resolu !!!!!!" << endl << endl;
            printNDmatrix(rND, constr);

        }else{
            cout << "fail" << endl;
        }
    }

    return 0;
}
