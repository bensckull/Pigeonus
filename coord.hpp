#ifndef COORD_H
#define COORD_H

#include <vector>
/*-------------------------------------------------------
    Classe pour gérer les coordonés pour la résolution
-------------------------------------------------------*/
class Coord{
    private:
        std::vector<int> coord;
        int nb;

    public:
        Coord(){
            nb = 0;
        }

        void setCoord(std::vector<int>);
        void add();
        int getNb();
        bool same(std::vector<int>);

};

void Coord::setCoord(std::vector<int> v){
    coord = v;

    nb = 1;
}

void Coord::add(){
    nb += 1;
}

int Coord::getNb(){
    return nb;
}

bool Coord::same(std::vector<int> v){
    return (v == coord);
}

#endif
