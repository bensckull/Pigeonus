#ifndef RESSOLV_H
#define RESSOLV_H

#include <vector>
#include <iterator>

using namespace std;
class Ressolv{

    private:
        std::vector<int> correctCol;
        std::vector<int> correctLine;
        bool resolved;

    public:
        Ressolv(){
            resolved = false;
        }

        bool addCol(int);
        bool addLine(int);
        void setResloved();
        bool isResolved();
        void printVect();
};

bool Ressolv::addCol(int col){
    if(std::find(correctCol.begin(), correctCol.end(), col)==correctCol.end()){
        correctCol.push_back(col);
        return true;
    }else{
        return false;
    }
}

bool Ressolv::addLine(int line){
    if(std::find(correctLine.begin(), correctLine.end(), line)==correctLine.end()){
        correctLine.push_back(line);
        return true;
    }else{
        return false;
    }
}

void Ressolv::setResloved(){
    resolved = true;
}

bool Ressolv::isResolved(){
    return resolved;
}

void Ressolv::printVect(){
    cout << "correctCol : ";
    for(auto v : correctCol){
        cout << v << " ";
    }
    cout << endl;

    cout << "correctLine : ";
    for(auto v : correctLine){
        cout << v << " ";
    }
    cout << endl;
}

#endif
