#ifndef COORDOPT_H
#define COORDOPT_H

#include <vector>
/*-------------------------------------------------------
    Classe pour gérer les coordonés pour la résolution
-------------------------------------------------------*/
class CoordOPT{
    private:
        std::vector<int> coord;
        int nb;

    public:
        CoordOPT(){
            nb = 0;
        }

        void setCoord(std::vector<int>);
        void add();
        int getNb();
        bool same(std::vector<int>);

};

void CoordOPT::setCoord(std::vector<int> v){
    coord = v;

    nb = 1;
}

void CoordOPT::add(){
    nb += 1;
}

int CoordOPT::getNb(){
    return nb;
}

bool CoordOPT::same(std::vector<int> v){
    return (v == coord);
}

#endif
