#!/bin/bash

dimensions="-c 5 0 1 0 5 -c 5 1 1 5 5"

echo -e "********************"
echo "Instance satisfiable"
echo -e "********************\n"


TYPE_METHODE=("NONE", "2 Dimensions", "Non parallélisée", "Parallélisée")

for method in {1..3} ; do
	if [ "$1" == "--quiet" ] ; then
		/usr/bin/time -f "> Methode ${method} : ${TYPE_METHODE[$method]} %e" bash -c "./test -m $method $dimensions >> /dev/null"
	else
		/usr/bin/time -f "> Methode ${method} : ${TYPE_METHODE[$method]} %e" ./test -m $method $dimensions
	fi
done

dimensions="-c 4 0 1 0 4 -c 5 1 1 5 5"

echo -e "\n**********************"
echo "Instance insatisfiable"
echo -e "**********************\n"

for method in {1..3} ; do
	if [ "$1" == "--quiet" ] ; then
		/usr/bin/time -f "> Methode ${method} : ${TYPE_METHODE[$method]} %e" bash -c "./test -m $method $dimensions >> /dev/null"
	else
		/usr/bin/time -f "> Methode ${method} : ${TYPE_METHODE[$method]} %e" ./test -m $method $dimensions
	fi
done
