#ifndef VECTCOORD_H
#define VECTCOORD_H

#include <vector>
#include "coord.hpp"

/*-------------------------------------------------------------------------------
    Classe pour gérer les coordonés des variables à "true" pour une dimmension
--------------------------------------------------------------------------------*/
using namespace std;
class VectCoordOPT{
    private:
        vector<Coord> vCoord;
        int minl;
        int maxl;
        int tot;


    public:
        VectCoordOPT(){
            minl = maxl = tot = 0;
        }

        void calculate();

        /*
            param v : vector of int with cood of others dim
        */
        void addCoord(std::vector<int>);

        int getMinL();

        int getMaxL();

        int getTotCount();

        int getUniqCount();

};

void VectCoordOPT::calculate(){

    minl = vCoord[0].getNb();
    maxl = minl;
    tot = 0;

    int temp;

    #pragma omp parallel for
    for(auto c : vCoord){

        temp = c.getNb();

        if(minl > temp){
            minl = temp;
        }
        if(maxl < temp){
            maxl = temp;
        }

        tot += temp;
    }
}

/*
    param v : vector of int with cood of others dim
*/
void VectCoordOPT::addCoord(std::vector<int> v){
;

    bool modif = false;

    #pragma omp parallel for
    for(int i = 0; i < (int)vCoord.size(); i++){

        if(vCoord[i].same(v)){

            vCoord[i].add();

            i = vCoord.size();

            modif = true;

        }
    }

    if(!modif){
        Coord c;
        c.setCoord(v);
        vCoord.push_back(c);

    }

    calculate();
}

int VectCoordOPT::getMinL(){
    return minl;
}

int VectCoordOPT::getMaxL(){
    return maxl;
}

int VectCoordOPT::getTotCount(){
    return tot;
}

int VectCoordOPT::getUniqCount(){
    return vCoord.size();
}

#endif
