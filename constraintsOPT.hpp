#ifndef CONSTROPT_H
#define CONSTROPT_H

#include <vector>

/*---------------------------------------------------------------------
    Classe pour gérer les contraintes et les dimensions de la matrice
---------------------------------------------------------------------*/

class ConstraintsOPT{

    private:
        //nombre de dimmesions por la matrice de résolution
        int nbDim;

        //nomres de nombre de variables dans la matrice
        int nbVar;

        //Nombre d'indices pour chaque dimmension
        std::vector<int> maxDim;

        //Nombre d'éléments total possible pour chaques dimm
        std::vector<int> maxTotDim;

        //taille d'un élémément pour chaque dimension
        std::vector<int> sizeDim;

        //nombre minimum de "true" par ligne
        std::vector<int> minOccur;
        //nombre maximum de "true" par ligne
        std::vector<int> maxOccur;

        //nombre maximum de "true" par dimmension
        //(par exemple nombre min de pigeons ou de pigeoniers a être affecté)
        std::vector<int> maxTrue;
        //nombre minimum de "true" par dimmension
        //(par exemple nombre max de pigeons ou de pigeoniers a être affecté)
        std::vector<int> minTrue;

    public:
        ConstraintsOPT(){

        }

        /*----------------------------------------------------
        Fonction d'initialisation pour les attributs :
            sizeDim
            maxTotDim
            nbVar

        ----------------------------------------------------*/
        void initSizeDim(){
            int temp3 = 1;

            #pragma omp parallel for
            for (int i = 0; i < nbDim; i++){
                int temp = 1;
                int temp2 = 1;

                for(int j = 0; j < nbDim; j++){
                    if(j>i){
                        temp *= maxDim[j];
                    }

                    if(j!=i){
                        temp2 *= maxDim[j];
                    }
                }

                sizeDim.push_back(temp);
                maxTotDim.push_back(temp2);
                temp3 *= maxDim[i];
            }

            nbVar = temp3;
        }

        /*------------------------------------------------------------------
        Fonction de conversion d'un indice d'un matrice à une dimmension
            à n indices pour une matrice à n dimmensions

        ------------------------------------------------------------------*/
        std::vector<int> convertToN(int n){
            std::vector<int> v;
            int temp;
            int rest = n;
            for(int i = 0; i< nbDim; i++){
                temp = rest/sizeDim[i];
                rest = rest%sizeDim[i];
                v.push_back(temp);
            }

            return v;

        }

        void setNbDim(int i){
            nbDim = i;
        }

        int getNbVar(){
            return nbVar;
        }

        int getNbDim(){
            return nbDim;
        }

        int getMaxDim(int i){
            return maxDim[i];
        }

        int getMaxTotDim(int i){
            return maxTotDim[i];
        }

        int getMaxOccur(int i){
            return maxOccur[i];
        }

        int getMinOccur(int i){
            return minOccur[i];
        }

        int getSizeDim(int i){
            return sizeDim[i];
        }

        void setMaxDim(int i){
            maxDim.push_back(i);
        }

        void setMaxOccur(int i){
            maxOccur.push_back(i);
        }

        void setMinOccur(int i){
            minOccur.push_back(i);
        }

        int getMaxTrue(int i){
            return maxTrue[i];
        }

        void setMaxTrue(int i){
            maxTrue.push_back(i);
        }

        int getMinTrue(int i){
            return minTrue[i];
        }

        void setMinTrue(int i){
            minTrue.push_back(i);
        }


};

#endif
