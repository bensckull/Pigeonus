#ifndef RESSOLVND_H
#define RESSOLVND_H

#include <vector>
#include <iterator>
#include "constraints.hpp"
#include "vectCoord.hpp"

using namespace std;
class RessolvND{

    private:

        std::vector<int> correctCol;
        std::vector<int> correctLine;

        std::vector<VectCoord> vCoord;
        Constraints constr;
        bool resolved;

    public:
        RessolvND(){
            resolved = false;
        }

        void setResloved();
        bool isResolved();
        void printVect();
        void setConstr(Constraints);
        bool add(int);
        bool testConstrMax(int);
        bool testConstrMin(int);
};

void RessolvND::setConstr(Constraints c){
    constr = c;

    /*init vCoord*/

    for(int i = 0; i < constr.getNbDim(); i++){

        VectCoord v;
        vCoord.push_back(v);
    }
}

bool RessolvND::testConstrMax(int dim){
    /*
        test MaxL
    */
    if(vCoord[dim].getMaxL() > constr.getMaxOccur(dim)){
        return false;
    }

    /*
        test MaxT
    */

    if(vCoord[dim].getTotCount() > constr.getMaxTrue(dim)){
        return false;
    }


    return true;
}

bool RessolvND::testConstrMin(int dim){

    /*
        test MinL
    */
    if(constr.getMinOccur(dim) > 0){

        /*si le nombre  de coord unique est différent du nombre de 'cases'
         maximales dans la ligne c'est que une des ligne ne comprends pas de 'true'*/

        if(vCoord[dim].getUniqCount() == constr.getMaxTotDim(dim)){

            if(constr.getMinOccur(dim) < vCoord[dim].getMinL()){

                return false;
            }

        }else{

            return false;
        }

    }

    /*
        test MinT
    */
    if(vCoord[dim].getTotCount() < constr.getMinTrue(dim)){

        return false;
    }

    return true;
}

bool RessolvND::add(int pos){

    std::vector<int> v = constr.convertToN(pos);

    int n = constr.getNbDim();

    bool res;

    for(int i = 0 ; i < n; i++){

        std::vector<int> vectTemp;

        /*creation du vecteur de coord correspondant à
        la dimmension courrante i */
        for (int j = 0; j < n; ++j)
        {

            if(i != j){
                vectTemp.push_back(v[j]);
            }

        }

        vCoord[i].addCoord(vectTemp);

        res = testConstrMax(i);

        if(res == false){
            return res;
        }

    }

    return true;
}


void RessolvND::setResloved(){

    int n = constr.getNbDim();

    bool res;

    for(int i = 0 ; i < n; i++){

        res = testConstrMin(i);

        if(res == false){
            resolved = false;

            return;
        }
    }

    resolved = true;
}

bool RessolvND::isResolved(){
    return resolved;
}

void RessolvND::printVect(){
    cout << "correctCol : ";
    for(auto v : correctCol){
        cout << v << " ";
    }
    cout << endl;

    cout << "correctLine : ";
    for(auto v : correctLine){
        cout << v << " ";
    }
    cout << endl;
}

#endif
